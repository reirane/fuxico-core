#!/usr/bin/python
# coding: utf-8
import os
import json

from bottle import Bottle, run, static_file, request, response, abort


UPLOAD_FOLDER = os.environ.get("UPLOADFOLDER")
app = Bottle()


@app.route("/hello", method="GET")
def hello():
    return "Hello World!"


@app.route("/Shared/<filepath:path>", method="DELETE")
def delete_file(filepath):
    if not os.path.exists(UPLOAD_FOLDER):
        print("No upload folder: %s" % UPLOAD_FOLDER)
        abort(404, "No upload folder set.")
    if filepath:
        print("filepath: %s" % filepath)
        path_to_delete = os.path.join(UPLOAD_FOLDER, filepath)
        if os.path.isfile(path_to_delete):
            print("path_to_delete %s is a file" % filepath)
            os.remove(path_to_delete)
    return "No Content"


@app.route("/Shared/upload/", method="POST")
def upload_file():
    upload = request.files.get("upload")
    if not os.path.exists(UPLOAD_FOLDER):
        print("No upload folder: %s" % UPLOAD_FOLDER)
        abort(500, "No upload folder set.")
    upload.save(UPLOAD_FOLDER)
    return "Created"


@app.route("/Shared/", method="GET")
@app.route("/Shared/<filepath:path>", method="GET")
def shared_files(filepath=None):
    if not os.path.exists(UPLOAD_FOLDER):
        print("No upload folder: %s" % UPLOAD_FOLDER)
        abort(404, "No upload folder set.")
    if filepath:
        print("filepath: %s" % filepath)
        if not os.path.exists(os.path.join(UPLOAD_FOLDER, filepath)):
            print("No filepath found: %s" % os.path.join(UPLOAD_FOLDER, filepath))
            abort(404, "No file found")
        if os.path.isfile(os.path.join(UPLOAD_FOLDER, filepath)):
            print("upload_path %s is a file" % filepath)
            return static_file(filepath, root=UPLOAD_FOLDER)
        upload_path = os.path.join(UPLOAD_FOLDER, filepath)

    print("upload_path %s is dir" % UPLOAD_FOLDER)
    response.content_type = "application/json"
    file_list = os.listdir(UPLOAD_FOLDER)
    return json.dumps(file_list)
