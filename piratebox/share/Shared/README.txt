
	</div>
</div>
<footer id="about">

        <div class="container">
                <p class="to-top"><a href="#header">Back to top</a></p>
                        <h2 data-l10n-id="footerAbout">Sobre a Sementeira</h2>
                      <p data-l10n-id="footerInspired">Essa é a Sementeira, um sistema livre de troca de arquivos local. Esse sistema não está na internet! Aqui você pode trocar arquivos e conversar de forma <strong> anônima</strong></p>
                <p>Entendemos cada arquivo como uma semente crioula*. Cada documento, foto, video, música ou conhecimento aqui compartilhado pode ser obtido livremente e "plantado" numa nova roça. Seja na sua <a href="http://fuxico.lan/sobre/suafuxico.html">própria fuxico</a> ou nos pen drives, celulares e computadores de quem passar por aqui. Acreditamos que o conhecimento, - assim como as sementes e as mulheres -, deve ser livre!</p>
<p>Liberdade, porém, envolve <a href="http://fuxico.lan/oi">consentir com o que você expõe</a> e nunca é a liberdade de oprimir a outras. Se algo te ofender, procure uma das responsáveis pela Fuxico. Ela vai poder avaliar e retirar esse conteúdo da sementeira.</p>
<br>
<p><i>*As sementes crioulas são aquelas cultivadas e mantidas pelos povos e comunidades tradicionais ao longo de gerações, perpetuando a riqueza natural de nossas terras. Por meio dos cultivos agroecológicos e das trocas de sementes, elas permanecem vivas.</i> (Extraído <a href="http://fuxico.lan/Shared/pitacos/%5bDOC%5dPraticas-feministas-portugu%c3%aas-SOF.pdf">desse manual</a> de práticas feministas produzido pela SOF - Semprelivre organização Feminista em 2018).</p>
                </div>
</div>
</footer>
