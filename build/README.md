# Parametros de Instalação

* SENHA_USUARIO: senha alfanumerica (obrigatória)
* SENHA_WIFI: senha alfanumerica (opcional)
* SENHA_ARQUIVOS: senha alfanumerica (opcional)
* HABILITAR_RODA: booleano
* TIPO_ARMAZENAMENTO: USB ou SDCARD ou BOTH
* CONTENT_SEMENTEIRA: all, autonomia, sexualidade, dicas, tenologias-digitais, saude, agroecologia ou none

## Configurar usando arquivo

Criar na mesma pasta do install o arquivo `.install.properties`:
```
SENHA_USUARIO=rootpass
SENHA_WIFI=wifipass
SENHA_ARQUIVOS=filepass
SENHA_RODA=senhaboard
HABILITAR_RODA=true
TIPO_ARMAZENAMENTO=SDCARD
CONTENT_SEMENTEIRA=all
```

## Configurar usando linha de comando

`SENHA_USUARIO=rootpass SENHA_WIFI=wifipass SENHA_ARQUIVOS=filepass SENHA_RODA=senhaboard HABILITAR_RODA=true TIPO_ARMAZENAMENTO=SDCARD CONTENT_SEMENTEIRA=ALL bash install.sh`